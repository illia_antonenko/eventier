<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    protected $fillable = ['start','end','title','description','user_id','category_id','repeat','published'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function eventRegistration(){
        return $this->hasMany(EventRegistration::class);
    }
}
