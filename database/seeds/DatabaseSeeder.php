<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'test@test.com',
            'role' => 'admin',
            'moderated'=>1,
            'email_verified_at' => now(),
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
        ]);
        DB::table('profiles')->insert([
            'user_id' => 1,
            'firstname'=>'Illia',
            'middlename'=>'',
            'lastname'=>'Antonenko',
            'nickname'=>'redrevan',
            'birthdate'=> strtotime('11.08.1997'),
            'hideyear'=>0,
            'phone'=>'+380999999999',
        ]);
        factory(\App\Models\Category::class,10)->create();
        factory(\App\Models\User::class,25)->create()->each(function ($user){
            $user->profile()->save(factory(\App\Models\Profile::class)->make());
            $user->news()->saveMany(factory(\App\Models\News::class,10)->make());
            $user->absence()->saveMany(factory(\App\Models\Absence::class,10)->make());
            $user->event()->saveMany(factory(\App\Models\Event::class,10)->make());
            $user->event()->each(function ($event){
                $event->eventRegistration()->saveMany(factory(\App\Models\EventRegistration::class,5)->make());
            });
        });

    }
}
